from setuptools import setup

setup(
    name="ceb-extract",
    version="0.1",
    description="Extracts data from CEB files",
    author="Peter Alping",
    py_modules=["src/cli", "src/extract", "src/query"],
    include_package_data=True,
    install_requires=["click", "cryptography"],
    entry_points="""
        [console_scripts]
        ceb=src.cli:cli
    """,
)
